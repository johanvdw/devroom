# FOSDEM 2022


## FOSS on mobile devices devroom

We are happy to announce the FOSS on mobile devices devroom at FOSDEM 22!

This devroom is about everything related to Linux and Free Software on Linux 
based smartphones (like the Librem 5 or the PinPhone) and other mobile devices 
running a regular (non-Android) Linux kernel. The topics can range from the 
very first bytes the bootloader runs, up to the 1 pixel offset you're fixing 
in your favorite toolkit. If you have been hacking, designing or reverse 
engineering in this field we would love to hear what you have been up to!

As FOSDEM will be an online event this year all talks will need to be
prerecorded and shared with FOSDEM organizers three weeks before the event
i.e. by January 16th 2022.


## Call for Participation

We are excited to invite the great communities around different projects
to submit their talks.

We are interested in seeing your demos, hearing your presentations and engaging
in lively discussions with developers and users.

Please bear in mind that while the talks are prerecorded, you must be available
online during the stream of your talk for the Q&A session.

If you're unsure if your proposed talk will be a good fit or if you have
other questions, feel free to reach out to us via our mailing list:
mobile-devroom-manager@fosdem.org


### Important dates

- 23-Dec-2021: Deadline for submissions
- 29-Dec-2021: Notification if your submission was accepted or not
- 31-Dec-2021: Schedule published
- 16-Jan-2021: Recorded talks uploaded
- 05-Feb-2021: Devroom taking place

All deadline times are 23:59 UTC.


## Topics

Some topics of interest including, but not limited to:

- Distributions for mobile devices
- Mainline device support
- Building hardware for mobile FOSS OS's
- Mobile software ecosystem (GNOME, Plasma Mobile, etc)
- Specific software projects


## Talk format

- Talks should be either 20 or 30 minutes long excluding Q&A
- For shorter talks (5 minutes) there will be a lightning talks session.
Slots are limited


## Submission details

Please use the following URL to submit a talk to FOSDEM 2022:

https://penta.fosdem.org/submission/FOSDEM22

- If you already have an account please use that one instead of creating
a new one
- Uploading a photograph and writing a short biography is encouraged
- You must include a contact email address
- Please don't submit your talk to multiple tracks, instead choose
  the most appropriate one
- Select "FOSS on mobile devroom" as the Track
- Add relevant information and try to be concise and descriptive
- Please include your timezone or preferred time slot in the
"Submission notes" field to make sure we can schedule your talk accordingly


## Licensing

This work is licensed under a
(Creative Commons Attribution 4.0 International License)[http://creativecommons.org/licenses/by/4.0/]
.
